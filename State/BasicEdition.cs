﻿using System;

namespace State.BasicEdition
{
    /// <summary>
    /// 生活
    /// </summary>
    public class Life
    {
        /// <summary>
        /// 时间段
        /// </summary>
        public Time Time { get; set; }

        /// <summary>
        /// 天气
        /// </summary>
        public Weather Weather { get; set; }

        /// <summary>
        /// 干啥子
        /// </summary>
        public void Doing()
        {
            if (Time == Time.Morning)
            {
                Console.WriteLine($"\n现在是 早上,春香 服侍我起床~");
            }
            else if (Time == Time.Noon)
            {
                Console.WriteLine($"\n现在是 中午,夏香 陪我玩耍~");
                if (Weather == Weather.Good)
                {
                    Console.WriteLine($"天气真好，陪我出去放风筝");
                }
                else
                {
                    Console.WriteLine($"天气不好，待家给我跳舞");
                }
            }
            else if (Time == Time.Afternoon)
            {
                Console.WriteLine($"\n现在是 下午,秋香 服侍我用餐~");
            }
            else if (Time == Time.Night)
            {
                Console.WriteLine($"\n现在是 晚上,冬香 服侍我就寝~");
            }
            else
            {
                Console.WriteLine($"\n睡觉中...");
            }
        }
    }
}