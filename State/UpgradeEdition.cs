﻿using System;

namespace State.UpgradeEdition
{
    /// <summary>
    /// 生活
    /// </summary>
    public class Life
    {
        private ITimeHandle _timeHandle;

        /// <summary>
        /// 时间段
        /// </summary>
        public Time Time { get; set; }

        /// <summary>
        /// 天气
        /// </summary>
        public Weather Weather { get; set; }

        /// <summary>
        /// 初始化 一天生活的 时间段
        /// </summary>
        public Life()
        {
            //默认设置时间为 上午
            _timeHandle = new MorningTime();
        }

        /// <summary>
        /// 设置时间段对应的操作类
        /// </summary>
        /// <param name="timeHandle"></param>
        public void SetTimeHandle(ITimeHandle timeHandle)
        {
            _timeHandle = timeHandle;
        }

        /// <summary>
        /// 干啥子
        /// </summary>
        public void Doing()
        {
            _timeHandle.Doing(this);
        }
    }

    public interface ITimeHandle
    {
        /// <summary>
        /// 干啥子
        /// </summary>
        /// <param name="life"></param>
        public void Doing(Life life);
    }

    /// <summary>
    /// 早上 干什么
    /// </summary>
    public class MorningTime : ITimeHandle
    {
        public void Doing(Life life)
        {
            if (life.Time == Time.Morning)
            {
                Console.WriteLine($"现在是 早上,春香 服侍我起床~");
            }
            else
            {
                life.SetTimeHandle(new NoonTime());
                life.Doing();
            }
        }
    }

    /// <summary>
    /// 中午 干什么
    /// </summary>
    public class NoonTime : ITimeHandle
    {
        public void Doing(Life life)
        {
            if (life.Time == Time.Noon)
            {
                Console.WriteLine($"现在是 中午,夏香 陪我玩耍~");
                if (life.Weather == Weather.Good)
                {
                    Console.WriteLine($"天气真好，陪我出去放风筝");
                }
                else
                {
                    Console.WriteLine($"天气不好，待家给我跳舞");
                }
            }
            else
            {
                life.SetTimeHandle(new AfternoonTime());
                life.Doing();
            }
        }
    }

    /// <summary>
    /// 下午 干什么
    /// </summary>
    public class AfternoonTime : ITimeHandle
    {
        public void Doing(Life life)
        {
            if (life.Time == Time.Afternoon)
            {
                Console.WriteLine($"现在是 下午,秋香 服侍我用餐~");
            }
            else
            {
                life.SetTimeHandle(new NightTime());
                life.Doing();
            }
        }
    }

    /// <summary>
    /// 晚上 干什么
    /// </summary>
    public class NightTime : ITimeHandle
    {
        public void Doing(Life life)
        {
            if (life.Time == Time.Night)
            {
                Console.WriteLine($"现在是 晚上,冬香 服侍我就寝~");
            }
            else
            {
                Console.WriteLine($"睡觉中...");
            }
        }
    }
}