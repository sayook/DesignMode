﻿using System;
using State.UpgradeEdition;

namespace State
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var life = new Life();
            //设置天气
            life.Weather = Weather.Good;

            life.Time = Time.Morning;
            life.Doing();

            life.Time = Time.Noon;
            life.Doing();

            life.Time = Time.Afternoon;
            life.Doing();

            life.Time = Time.Night;
            life.Doing();

            Console.WriteLine("\nHappy Ending~");
            Console.ReadLine();
        }
    }
}