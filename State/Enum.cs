﻿namespace State
{
    /// <summary>
    /// 时间段
    /// </summary>
    public enum Time
    {
        Morning,
        Noon,
        Afternoon,
        Night
    }

    /// <summary>
    /// 天气
    /// </summary>
    public enum Weather
    {
        /// <summary>
        /// 好天气
        /// </summary>
        Good,

        /// <summary>
        /// 坏天气
        /// </summary>
        Bad
    }
}