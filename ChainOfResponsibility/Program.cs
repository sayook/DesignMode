﻿using System;
using UpgradeEdition;

namespace ChainOfResponsibility
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //创建领导
            var pm = new PM(ManagerType.PM);
            var cto = new CTO(ManagerType.CTO);
            var ceo = new CEO(ManagerType.CEO);
            //设置下一级处理者
            pm.SetSuccessor(cto);
            cto.SetSuccessor(ceo);

            //创建 请假请求
            var request1 = new Requset
            {
                Type = RequestType.Leave,
                Number = 5
            };
            pm.Process(request1);
            Console.WriteLine("\n");

            //创建 加薪请求
            var request2 = new Requset
            {
                Type = RequestType.PayRise,
                Number = 2000
            };
            pm.Process(request2);

            Console.WriteLine("\nHappy Ending~");
            Console.ReadLine();
        }
    }
}