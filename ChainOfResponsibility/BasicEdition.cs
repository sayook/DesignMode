﻿using System;
using ChainOfResponsibility;

namespace BasicEdition
{
    public class Manager
    {
        private readonly ManagerType _managerType;

        public Manager(ManagerType managerType)
        {
            _managerType = managerType;
        }

        /// <summary>
        /// 处理请求
        /// </summary>
        public void Process(Requset requset)
        {
            if (_managerType == ManagerType.PM)
            {
                if (requset.Type == RequestType.Leave && requset.Number <= 2)
                {
                    Console.WriteLine($"项目经理 已批准 你的 {requset.Content} 申请");
                }
                else
                {
                    Console.WriteLine($"项目经理 无权批准 你的 {requset.Content} 申请");
                }
            }
            else if (_managerType == ManagerType.CTO)
            {
                if (requset.Type == RequestType.Leave)
                {
                    if (requset.Number <= 5)
                    {
                        Console.WriteLine($"CTO 已批准 你的 {requset.Content} 申请");
                    }
                    else
                    {
                        Console.WriteLine($"CTO 无权批准 你的 {requset.Content} 申请");
                    }
                }
                else
                {
                    if (requset.Number <= 500)
                    {
                        Console.WriteLine($"CTO 已批准 你的 {requset.Content} 申请");
                    }
                    else
                    {
                        Console.WriteLine($"CTO 无权批准 你的 {requset.Content} 申请");
                    }
                }
            }
            else if (_managerType == ManagerType.CEO)
            {
                if (requset.Type == RequestType.Leave)
                {
                    Console.WriteLine($"CEO 已批准 你的 {requset.Content} 申请");
                }
                else
                {
                    if (requset.Number <= 1000)
                    {
                        Console.WriteLine($"CEO 已批准 你的 {requset.Content} 申请");
                    }
                    else
                    {
                        Console.WriteLine($"CEO对你的 {requset.Content} 申请 说：“小子，你有点飘啊！”");
                    }
                }
            }
        }
    }
}