﻿namespace ChainOfResponsibility
{
    /// <summary>
    /// 请求
    /// </summary>
    public class Requset
    {
        /// <summary>
        /// 请求类型
        /// </summary>
        public RequestType Type { get; set; }

        /// <summary>
        /// 请求数量
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// 请求说明
        /// </summary>
        public string Content
        {
            get
            {
                switch (Type)
                {
                    case RequestType.Leave:
                        return $"请假{Number}天";

                    case RequestType.PayRise:
                        return $"加薪{Number}元";

                    default:
                        return "未知";
                }
            }
        }
    }

    /// <summary>
    /// 请求类型
    /// </summary>
    public enum RequestType
    {
        /// <summary>
        /// 请假
        /// </summary>
        Leave,

        /// <summary>
        /// 加薪
        /// </summary>
        PayRise
    }

    /// <summary>
    /// 管理者类型
    /// </summary>
    public enum ManagerType
    {
        PM,
        CTO,
        CEO
    }
}