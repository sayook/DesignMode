﻿using System;
using ChainOfResponsibility;

namespace UpgradeEdition
{
    public abstract class Manager
    {
        protected readonly ManagerType _managerType;
        protected Manager Successor { get; private set; }

        public Manager(ManagerType managerType)
        {
            _managerType = managerType;
        }

        /// <summary>
        /// 设置下一个处理者
        /// </summary>
        /// <param name="manager"></param>
        public void SetSuccessor(Manager manager)
        {
            Successor = manager;
        }

        /// <summary>
        /// 处理请求
        /// </summary>
        /// <param name="requset"></param>
        public abstract void Process(Requset requset);
    }

    public class PM : Manager
    {
        public PM(ManagerType managerType) : base(managerType)
        {
        }

        public override void Process(Requset requset)
        {
            if (requset.Type == RequestType.Leave && requset.Number <= 2)
            {
                Console.WriteLine($"项目经理 已批准 你的 {requset.Content} 申请");
                return;
            }

            if (Successor != null)
            {
                //交由下一级处理
                Successor.Process(requset);
            }
        }
    }

    public class CTO : Manager
    {
        public CTO(ManagerType managerType) : base(managerType)
        {
        }

        public override void Process(Requset requset)
        {
            if (requset.Type == RequestType.Leave && requset.Number <= 5)
            {
                Console.WriteLine($"CTO 已批准 你的 {requset.Content} 申请");
                return;
            }

            if (requset.Type == RequestType.PayRise && requset.Number <= 500)
            {
                Console.WriteLine($"CTO 已批准 你的 {requset.Content} 申请");
                return;
            }
            if (Successor != null)
            {
                //交由下一级处理
                Successor.Process(requset);
            }
        }
    }

    public class CEO : Manager
    {
        public CEO(ManagerType managerType) : base(managerType)
        {
        }

        public override void Process(Requset requset)
        {
            if (requset.Type == RequestType.Leave && requset.Number <= 15)
            {
                Console.WriteLine($"CEO 已批准 你的 {requset.Content} 申请");
                return;
            }

            if (requset.Type == RequestType.PayRise && requset.Number <= 1000)
            {
                Console.WriteLine($"CEO 已批准 你的 {requset.Content} 申请");
                return;
            }
            Console.WriteLine($"CEO对你的 {requset.Content} 申请 说：“小子，你有点飘啊！”");
        }
    }
}