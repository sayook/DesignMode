﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Prototype
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var sw = new Stopwatch();
            sw.Start();
            var beauty1 = new Beauty(82, 54, 86);
            sw.Stop();
            Console.WriteLine($"生产第一名美女耗时：{sw.ElapsedMilliseconds}/ms\n");
            beauty1.SetName("秋香");
            beauty1.SetTalent("弹琴", 10);

            sw.Restart();
            var beauty2 = (Beauty)beauty1.Clone();
            sw.Stop();
            Console.WriteLine($"生产第二名美女耗时：{sw.ElapsedMilliseconds}/ms\n");
            beauty2.SetName("春香");
            beauty2.SetTalent("画画", 9);

            sw.Restart();
            var beauty3 = (Beauty)beauty1.Clone();
            sw.Stop();
            Console.WriteLine($"生产第三名美女耗时：{sw.ElapsedMilliseconds}/ms\n");
            beauty3.SetName("夏香");
            beauty3.SetTalent("舞蹈", 8);

            Show(beauty1);
            Show(beauty2);
            Show(beauty3);

            Console.WriteLine("\nHappy Ending~");
            Console.ReadLine();
        }

        public static void Show(Beauty beauty)
        {
            Console.WriteLine($"我是 {beauty.Name},身材[{beauty.Bust}-{beauty.TheWaist}-{beauty.Hipline}],我的才艺是 {beauty.Talent.Name} 段位 {beauty.Talent.Level}");
        }
    }

    public class Beauty : ICloneable
    {
        public Beauty(int bust, int theWaist, int hipline)
        {
            Bust = bust;
            TheWaist = theWaist;
            Hipline = hipline;
            Talent = new Talent();
            //模拟产生一名美人的时长
            Thread.Sleep(3000);
        }

        private Beauty(Talent talent)
        {
            this.Talent = (Talent)talent.Clone();
        }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 才艺
        /// </summary>
        public Talent Talent { get; set; }

        /// <summary>
        /// 胸围
        /// </summary>
        public int Bust { get; set; }

        /// <summary>
        /// 腰围
        /// </summary>
        public int TheWaist { get; set; }

        /// <summary>
        /// 臀围
        /// </summary>
        public int Hipline { get; set; }

        /// <summary>
        /// 起名
        /// </summary>
        /// <param name="name"></param>
        public void SetName(string name)
        {
            Name = name;
        }

        /// <summary>
        /// 设置才艺
        /// </summary>
        /// <param name="talent"></param>
        public void SetTalent(string name, int level)
        {
            Talent.Name = name;
            Talent.Level = level;
        }

        public object Clone()
        {
            //调用私有构造方法，让“才艺”克隆完成，然后再给这个 “美人” 对象的相关字段赋值，
            //最终返回一个深复制的 “美人” 对象
            var beauty = new Beauty(Talent)
            {
                Bust = this.Bust,
                TheWaist = this.TheWaist,
                Hipline = this.Hipline
            };
            return beauty;
        }
    }

    public class Talent : ICloneable
    {
        /// <summary>
        /// 才艺名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 段位
        /// </summary>
        public int Level { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}