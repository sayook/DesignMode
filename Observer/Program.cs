﻿using System;
using System.Threading.Tasks;

namespace Observer
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            //var subject = new ObserverAsync.PaySuccessSubject();
            //var observer1 = new ObserverAsync.CouponObserver(subject);
            //var observer2 = new ObserverAsync.MessageObserver(subject);
            //var observer3 = new ObserverAsync.BonusObserver(subject);

            var subject = new ObserverDelegate.PaySuccessSubject();
            var observer1 = new ObserverDelegate.CouponObserver(subject);
            var observer2 = new ObserverDelegate.MessageObserver(subject);
            var observer3 = new ObserverDelegate.BonusObserver(subject);
            //添加订阅
            //subject.Attach(observer1);
            //subject.Attach(observer2);
            //subject.Attach(observer3);

            subject.Update += observer1.MakeCouponAsync;
            subject.Update += observer2.SendMessageAsync;
            subject.Update += observer3.AddBonusAsync;
            //发布通知
            subject.State = "星巴克10十元券采购成功";
            await subject.Notify();

            Console.WriteLine("\n\nHappy Ending~");
            Console.ReadLine();
        }
    }
}